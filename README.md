Hey all, the mailing lists crapped out so I'm making this repo as a place for discussion on this question:

  What does "hacker culture" mean to you?

I think this is a really important question, as it affects
  - what kinds of services we want to host
  - ad policies
  - rules about what content is/isn't allowed
  - board topics
  - site aesthetic

Please clone this repo, add a file \<your_irc_username\>.txt, and push it here.

Obviously, the mailing list would be a better place for this, but it seems to have
crapped out (some people aren't on the mailing lists yet, either).


-ryu
